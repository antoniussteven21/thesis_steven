\babel@toc {english}{}\relax 
\contentsline {chapter}{Approval Page}{i}{section*.2}%
\contentsline {chapter}{EXAMINERS APPROVAL PAGE}{ii}{section*.3}%
\contentsline {chapter}{Statement by The Author}{iii}{section*.4}%
\contentsline {chapter}{Abstract}{iv}{section*.5}%
\contentsline {chapter}{Acknowledgements}{v}{section*.6}%
\contentsline {chapter}{Contents}{vi}{section*.13}%
\contentsline {chapter}{List of Figures}{ix}{section*.14}%
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.19}%
\contentsline {section}{\numberline {1.1}Background}{1}{section.20}%
\contentsline {section}{\numberline {1.2}Problem Statement}{4}{section.25}%
\contentsline {section}{\numberline {1.3}Research Objective}{5}{section.27}%
\contentsline {section}{\numberline {1.4}Research Scope}{7}{section.29}%
\contentsline {chapter}{\numberline {2}Literature Review}{8}{chapter.34}%
\contentsline {section}{\numberline {2.1}Previous Work on Flow around Square Cylinders}{8}{section.35}%
\contentsline {section}{\numberline {2.2}Governing Equations}{10}{section.36}%
\contentsline {subsection}{\numberline {2.2.1}Continuity Equation}{10}{subsection.37}%
\contentsline {subsection}{\numberline {2.2.2}Momentum Equation}{12}{subsection.48}%
\contentsline {section}{\numberline {2.3}Background Concept}{19}{section.92}%
\contentsline {subsection}{\numberline {2.3.1}Reynolds Number}{20}{subsection.93}%
\contentsline {subsection}{\numberline {2.3.2}Drag Coefficient}{20}{subsection.95}%
\contentsline {subsection}{\numberline {2.3.3}Lift Coefficient}{20}{subsection.97}%
\contentsline {subsection}{\numberline {2.3.4}Turbulence}{21}{subsection.99}%
\contentsline {subsection}{\numberline {2.3.5}Karman Vortex Street}{22}{subsection.104}%
\contentsline {subsubsection}{\numberline {2.3.5.1}Strouhal Number}{23}{subsubsection.106}%
\contentsline {section}{\numberline {2.4}CFD Discretization}{24}{section.108}%
\contentsline {subsection}{\numberline {2.4.1}Geometric and Physical Modeling}{24}{subsection.113}%
\contentsline {subsection}{\numberline {2.4.2}Domain Discretization}{25}{subsection.115}%
\contentsline {subsection}{\numberline {2.4.3}Equation Discretization}{26}{subsection.118}%
\contentsline {subsection}{\numberline {2.4.4}Solution of the Discretized Equations}{28}{subsection.123}%
\contentsline {subsubsection}{\numberline {2.4.4.1}Direct Methods}{28}{subsubsection.124}%
\contentsline {subsubsection}{\numberline {2.4.4.2}Iterative Methods}{28}{subsubsection.126}%
\contentsline {section}{\numberline {2.5}Machine Learning Application}{29}{section.132}%
\contentsline {chapter}{\numberline {3}Research Methodology}{32}{chapter.134}%
\contentsline {section}{\numberline {3.1}Overview}{32}{section.135}%
\contentsline {section}{\numberline {3.2}Computational Fluid Dynamics (CFD)}{34}{section.142}%
\contentsline {subsection}{\numberline {3.2.1}OpenFOAM}{34}{subsection.143}%
\contentsline {section}{\numberline {3.3}Model Generation}{35}{section.145}%
\contentsline {section}{\numberline {3.4}Preprocessing}{38}{section.151}%
\contentsline {section}{\numberline {3.5}Simulation}{40}{section.154}%
\contentsline {subsection}{\numberline {3.5.1}RANS $\mathit {k}$-$\omega $ SST Turbulence Model}{41}{subsection.156}%
\contentsline {subsubsection}{\numberline {3.5.1.1}RANS $\mathit {k}$-$\omega $ SST Model $\mathit {k}$ Parameter}{42}{subsubsection.161}%
\contentsline {subsubsection}{\numberline {3.5.1.2}RANS $\mathit {k}$-$\omega $ SST Model $\omega $ Parameter}{42}{subsubsection.163}%
\contentsline {section}{\numberline {3.6}Postprocessing}{42}{section.165}%
\contentsline {chapter}{\numberline {4}Result and Discussion}{44}{chapter.166}%
\contentsline {section}{\numberline {4.1}Convergence}{44}{section.167}%
\contentsline {subsection}{\numberline {4.1.1}Aerodynamic Coefficients}{50}{subsection.173}%
\contentsline {section}{\numberline {4.2}Mean Velocity}{51}{section.174}%
\contentsline {section}{\numberline {4.3}Velocity Functions}{57}{section.180}%
\contentsline {subsection}{\numberline {4.3.1}Streamwise Velocity Function ($u_{rms}$)}{57}{subsection.181}%
\contentsline {subsection}{\numberline {4.3.2}Normal Velocity Function ($v_{rms}$)}{63}{subsection.187}%
\contentsline {section}{\numberline {4.4}Pressure Distribution}{69}{section.193}%
\contentsline {section}{\numberline {4.5}Strouhal Number}{75}{section.199}%
\contentsline {chapter}{\numberline {5}Conclusion and Recommendation}{77}{chapter.201}%
\contentsline {section}{\numberline {5.1}Conclusion}{77}{section.202}%
\contentsline {section}{\numberline {5.2}Recommendation}{78}{section.203}%
\contentsline {chapter}{Bibliography}{79}{chapter*.204}%
\contentsline {chapter}{Appendices}{84}{section*.205}%
