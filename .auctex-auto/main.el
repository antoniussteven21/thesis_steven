(TeX-add-style-hook
 "main"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("KakiThesis" "	12pt" "	oneside" "	english" "	onehalfspacing" "							headsepline" "		")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("biblatex" "backend=bibtex" "style=ieee" "sorting=none" "natbib=true") ("inputenc" "utf8")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "Chapters/Chapter1"
    "Chapters/Chapter2"
    "Chapters/Chapter3"
    "Chapters/Chapter4"
    "Chapters/Chapter5"
    "Appendices/AppendixA"
    "KakiThesis"
    "KakiThesis10"
    "biblatex"
    "subcaption"
    "adjustbox"
    "siunitx"
    "txfonts"
    "pxfonts"
    "amsmath"
    "svg"
    "listings"
    "xcolor"
    "hyperref"
    "pdfpages"
    "inputenc")
   (TeX-add-symbols
    "varv")
   (LaTeX-add-bibliographies
    "References")
   (LaTeX-add-listings-lstdefinestyles
    "mystyle")
   (LaTeX-add-xcolor-definecolors
    "codegreen"
    "codegray"
    "codepurple"
    "backcolour"))
 :latex)

