
# Table of Contents

1.  [<code>[2/3]</code>](#org49a03ef)
2.  [How to use the template:](#orgdb24ec4)
3.  [How to compile your thesis:](#org90ac261)
4.  [Tips:](#org0b7a674)
5.  [Notes:](#org34e1706)

This repo is a LaTeX thesis template for Dagobah Advanced Institute of Technology (DAIT).

The thesis template from very far way universities, maybe a lot less than desirable, either aesthetically, readability, and usability.

The students from that kind of universities may adopt this template for their needs.

I put a lot of man-hours to create this template, so you don&rsquo;t have too. However it&rsquo;s not official and it&rsquo;s not perfect.


<a id="org49a03ef"></a>

# TODO <code>[2/3]</code>

-   [-] Adopt the latest template
    -   [X] Main structures
    -   [X] Title page
    -   [X] Layout style
    -   [X] Fonts
    -   [ ] Refinements
-   [X] Write short &ldquo;How to Use.&rdquo;
-   [X] Add CI


<a id="orgdb24ec4"></a>

# How to use the template:

1.  Create an account GITLAB
2.  Create a repo and name it Your-Name-Class-Year-Thesis-Manuscript, e.g. Luke-Skywalker-1978-Thesis-Manuscript
3.  Edit the main.tex:
    -   Thesis Title
    -   Keywords
    -   Supervisor
    -   Co-supervisor (optional); if you don&rsquo;t have co-supervisor, deactivate the part in the approval page.
    -   Author
    -   Subject (optional)
    -   University
    -   Department
    -   Faculty
    -   Dean
    -   When you completed your thesis defense, edit the examiners&rsquo; name accordingly.
4.  Replace the university logo with your university&rsquo;s in the folder Figures, you can use the same name (jpg/png); if you want to use different name, make sure to change the name and scale in the title page in the main.tex.
5.  Write you thesis the tex files in the folder of Chapters.
6.  Replace the references.bib with yours.
7.  Place the Turnitin report file in the folder of Turnitin<sub>Report</sub>.
8.  Place your code in the Appendices, see the examples.
9.  Edit the appendix files as necessary.
10. Place your photo in the folder MyCV.
11. Edit the file mycv.tex in the folder of MyCV as necessary.


<a id="org90ac261"></a>

# How to compile your thesis:

-   This template is tested using Carlito and LuaLateX (lualatex -shell-escape main.tex), if you use XeTeX or Calibrii, you may need to tweaks some minor parts in the main.tex, e.g. the scale of the logo.
-   If you use Linux or Mac, edit the font part in the main.tex accordingly to meet your OS. Make sure you have install carlito font, e.g. in Ubuntu: sudo apt install -y fonts-crosextra-carlito. You then can compile it using PdfLaTeX/LuaLaTeX. Carlito font is an open source drop-in replacement for Calibri font, however as both fonst are ugly for long and dense documents, so it&rsquo;s better not to use it.
-   If you use Windows, edit the font part in the main.tex and compile it with XeTeX.
-   If you use Linux/Mac, you can also use Calibri font directly, by downloading the fonts, but you must compile/build it using XeTeX or LuaLaTeX.


<a id="org0b7a674"></a>

# Tips:

-   When using citation in caption, use \protect, e.g. \caption{Base on Einstein \protect\cite{paper:einstein1915}}
-   To work nicely with apacite, make sure all the entries of your references has year field.


<a id="org34e1706"></a>

# Notes:

-   Consider migrating from apacite to [apa7](https://ctan.org/pkg/apa7?lang=en)

