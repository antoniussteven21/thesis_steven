\babel@toc {english}{}\relax 
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Background}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Problem Statement}{6}{0}{1}
\beamer@subsectionintoc {1}{3}{Research Objectives}{8}{0}{1}
\beamer@subsectionintoc {1}{4}{Research Scope}{9}{0}{1}
\beamer@sectionintoc {2}{Research Methodology}{10}{0}{2}
\beamer@sectionintoc {3}{Results and Discussion}{19}{0}{3}
\beamer@sectionintoc {4}{Conclusions and Recommendations}{73}{0}{4}
