% Chapter 1

\chapter{Introduction}
\label{Chapter1}

%---------------------------------------------------------------------
\section{Background} \label{section_background}
% ---------------------------------------------------------------------
Fluid flows occur in various engineering applications and also in a wide range of phenomena. In addition, its boundary conditions and initial conditions dictate its dynamics. In most cases, the dynamics of fluid flows are so complex that the development and implementation of flow models become essential in order to understand them better. The modeling of these flows needs certain parameters and variables, which include the state of the fluid (e.g. liquid, gas), fluid variables (e.g. compressibility, viscosity), and external conditions (e.g. boundary condition, initial condition) \cite{rujun_liu}.

Fluid flows can be classified into two categories: laminar flow and turbulent flow. Fluids in laminar flow have smooth streamlines, on the other hand, fluids in turbulent flow move randomly. Furthermore, laminar flows are associated with a low Reynolds number, while turbulent flows are associated with a high Reynolds number. To be more specific, fluid flows are said to be laminar when $Re < Re_{cr}$ and turbulent when $Re > Re_{cr}$. Here, the critical Reynolds number is the transition point where the flow that was previously laminar becomes turbulent. In real life, turbulent flow is the dominant type of flow that we deal with on a daily basis. Due to this phenomenon, it is currently impossible to have analytical solutions for turbulent flows, and as a result, CFD is used to solve this problem \cite{rujun_liu}.

Bluff bodies are objects that have a non-streamlined shape. Fluids flowing through it exhibit flow separation, where there is a sudden breakaway of the boundary layer from the surface of the body, which results in a thick trailing wake. This will lead to a friction drag due to viscous effects that is less than the pressure drag on the body. Some examples of bluff bodies are cylinders, cuboids, and pyramids \cite{aman_mohd}.

It is crucial to study the flow around bluff bodies because most objects in the real world are made up of bluff bodies instead of streamlined bodies. A simple example can be seen in vehicles and structures. By learning the flow around them, it could help engineers lower fuel consumption or prolong structure life to prevent early mechanical failure \cite{rujun_liu}.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{Figures/building_picture}
	\caption{Bluff Body Example (Structure)}
	\label{building_picture}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{Figures/car_picture}
	\caption{Bluff Body Example (Vehicle)}
	\label{car_picture}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.23]{Figures/world_trade_center.png}
	\caption{Bluff Body Side-by-Side (World Trade Center USA)}
	\label{world_trade_center}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.115]{Figures/pantograph.png}
	\caption{Train Pantograph}
	\label{train_pantograph}
\end{figure}


%---------------------------------------------------------------------
\section{Problem Statement}
%---------------------------------------------------------------------
As mentioned in section \ref{section_background}, the phenomena of fluid flows around bluff bodies happens everywhere in real life and it is crucial to study them as it could help to improve safety and efficiency in certain fields. Although geometrically simple, the flow physics and aerodynamic forces around a single bluff body are highly dependent of fluid properties, such as velocity, density, viscosity, and etc. In practical application, knowing the flow characteristics around objects can help in engineering design, such as in study of aeroacoustic, vortex-induced vibration, and etc. For example, a resonance or vibration phenomenon will be avoided if we know the frequency of von Karman vortex street behind a configuration. As an engineer, we do not want a natural frequency of one configuration that is amplified due to other physics phenomenon.


In low-subsonic region, the flow characteristics past a cylinder depend on Reynold number $Re$ and Mach number $M$. For multiple configuration, i.e. two, three, and more bluff bodies in side-by-side or tandem arrangements, the complexity arises due to the gap or distance between bodies. For two bodies in side-by-side arrangement, the flow characteristics are varied and categorized in three major regimes, such as single-like, flip-flop, and symmetric flow depend on the vortex formation as shown in Fig.\ref{ch1-flowregime} \cite{kang_characteristics_2003}.



\begin{figure}[H]
\centering
\begin{minipage}{.33\textwidth}
  \centering
  \includegraphics[width=1.\linewidth]{Figures/ppt_single.png}
\end{minipage}%
\begin{minipage}{.33\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{Figures/ppt_flipflop.png}
\end{minipage}
\begin{minipage}{.33\textwidth}
  \centering
  \includegraphics[width=1.\linewidth]{Figures/ppt_antiphase.png}
\end{minipage}
\caption{Single-like, flip-flop, and symmetric flow behaviour at low Reynolds numbers $Re<160$ \cite{kang_characteristics_2003}.}
\label{ch1-flowregime}
\end{figure}

One of the methods to study the bluff bodies flow in side-by-side arrangement is computational fluid dynamics (CFD). Various turbulence model were developed to  simulate the flow characteristics in the real physics, from RANS (Reynolds - Averaged Navier Stokes) to DNS (Direct Numerical Simulation) turbulence model. Not just that, the numerical solver is also constantly  developed to be widely available for solving various flow phenomenon.  Unlike RANS, the DNS model is computationally expensive, although it is more accurate. Therefore, in this research the two bluff bodies, i.e. square cylinders are studied using RANS model to see the flow characteristics and aerodynamics forces.

%Furthermore, the flow dynamics around these objects are  complex that it requires the help of a computer software designed to do the computing, which is the aim of this study.

%---------------------------------------------------------------------
\section{Research Objective}
\label{research_objective}
%---------------------------------------------------------------------
The purpose of this study is to obtain insights about the flow characteristics of air when passing through two square cylinders in a side-by-side arrangement subjected to a variety of combinations of $\mathit{L/D}$ and $\mathit{Re}$ using the $k - \omega$ SST turbulence model in OpenFOAM where $\mathit{Re}$ is the Reynolds Number, which can be seen in subsection \ref{subsection_re} for more details. 

\begin{figure}[H]
	\centering
	\includesvg[scale=0.4]{Figures/inoue_gr.svg}
	\caption{Gap Ratio Definition}
	\label{gap_ratio}
\end{figure}


Figure \ref{gap_ratio} is the definition of the gap ratio. where $L$ is the distance between the two square cylinders represented by the red line while $D$ is the diameter of the square cylinder. In addition, The open-source software OpenFOAM is used to conduct the simulation using $k-\omega$ SST model.

A side objective of this study is to provide training data to be used for deep learning in order to see if the results obtained from deep learning are close to the results obtained from a other CFD simulation using other turbulence model. If the results obtained from deep learning are not that different from the results obtained from CFD, it means that deep learning can be used as a second option to simulate this kind of study since the runtime would be much faster than CFD. If the results obtained from deep learning are different from the results obtained from CFD, the next step would be to investigate whether the results from deep learning can be used as the initial input into the CFD model in order to speed up the simulation. The main purpose of the side goal is to cut down simulation time as much as possible while obtaining an accurate result.

\pagebreak
%---------------------------------------------------------------------
\section{Research Scope}
%---------------------------------------------------------------------
The scope of this study are:

\begin{enumerate}
	\item The study is conducted on 2D two square cylinders in side-by-side arrangement at 5 $Re$ values ranging from $Re = 100 - \num{2.0e4}$ and $L/D = 1 - 7$ with increments of 0.5 only.
	\item The freestream velocity and the size of the square cylinders are kept constant.
	\item The study was done in incompressible condition ($M < 0.3$).
	\item The study focuses on the contours of $u_{mean}, u_{rms}, v_{rms}$, and $\mathit{p}$, the variation of $\mathit{St}$ against $\mathit{L/D}$, and the behavior of the aerodynamic coefficients $\mathit{C_L}$ and $\mathit{C_D}$
\end{enumerate}
