% Chapter 3

\chapter{Research Methodology}
\label{Chapter3} 

%---------------------------------------------------------------------
\section{Overview}
\label{overview}
% ---------------------------------------------------------------------

Due to the complex nature of the problem, computational fluid dynamics (CFD) will be utilized in order to conduct simulations at different combinations of Reynolds numbers and gap ratios, as mentioned in Chapter \ref{Chapter1}. Moreover, CFD also provides detailed information regarding the parameters that are to be observed.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.3]{Figures/ppt_workflow.png}
	\caption{Research Work Flow}
	\label{research_workflow}
\end{figure}

\pagebreak
In order to conduct this study, several steps have to be done as follows:

\begin{enumerate}
	\item Model Generation
	\item Preprocessing
	\item Simulation
	\item Postprocessing
	\item Analysis and Results 
\end{enumerate}

Model generation is essential to any CFD simulation, because if the model is not present, the CFD software does not have anything to work with. This model generation step is where various square cylinders in side-by-side arrangement models will be created with different gap ratios. 

The second step is preprocessing. This step includes initial condition selection, which is important because it gives the CFD software a clue on the starting point for the simulation in order to obtain the result of interest. This step also includes the solver selection for the CFD software to use for its computation. 

The third step is the simulation step. This is where the solver that was chosen previously on the second step is used to conduct iterative calculations until the result reaches convergence. 

The fourth step is postprocessing. This is the step where the results obtained from the simulation are further modified or transformed in a way that helps users understand better what was happening to the flow. This includes creating time series graphs or visualizing the fluid flow using a visualization tool.

The final step is data analysis and results. This is the step where the results are analyzed to obtain insights into what was happening to the flow around the two square cylinders, and it is also the step where the results are reported.
\pagebreak

%---------------------------------------------------------------------
\section{Computational Fluid Dynamics (CFD)}
\label{section_cfd}
% ---------------------------------------------------------------------
Computational fluid dynamics (CFD) is a field that utilizes digital computers to generate quantitative forecasts of fluid flow by applying the principles of conserving mass, momentum, and energy. It utilizes numerical methods to convert partial differential equations into a system of algebraic equations and is a quick and cheap way to obtain useful information. However, one must be cautious when interpreting the results, as there are many potential sources of error involved, such as modeling error, input data error, and initial condition error. Nevertheless, all of this can be avoided if one is careful in preparing the models before running the simulation \cite{hu_cfd}.

%---------------------------------------------------------------------
\subsection{OpenFOAM}
% ---------------------------------------------------------------------

Open Source Field Operation and Manipulation, commonly referred to as OpenFOAM, is a C++ library that serves as the foundation for creating executable applications. These applications can be classified into two categories: solvers and utilities. Solvers are specifically designed to address particular problems in continuum mechanics. On the other hand, utilities are developed by users who possess a fundamental understanding of the underlying methodology, physics, and programming. Additionally, OpenFOAM includes preprocessing and postprocessing environments, and the interface to these environments is facilitated by OpenFOAM utilities, ensuring consistent data management across all environments. Figure \ref{op_structure} below shows the overall structure of OpenFOAM \cite{openfoam_intro_one}.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.3]{Figures/of_workflow.png}
	\caption{OpenFOAM Structure}
	\label{op_structure}
\end{figure}

For this study, OpenFOAM will be utilized for the simulation of the different types of models with different gap ratios that were defined previously in Chapter \ref{Chapter1}. To be precise, the version of OpenFOAM that will be used is OpenFOAM v10.

%---------------------------------------------------------------------
\section{Model Generation}
% ---------------------------------------------------------------------

The model generation, also known as mesh generation in OpenFOAM, is done through the utility blockMeshDict. This is basically a file that users use to define the coordinates of each vertex of a model in space. It also allows them to define the blocks of the model as well as its faces. In addition, the blockMeshDict file also permits users to control how fine or coarse their models are. An example of the blockMeshDict utility can be seen in Figure \ref{of_blockmeshdict} (note that Figure \ref{of_blockmeshdict} is only an example and not the original file used to generate the model in this study).

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{Figures/openfoam_blockmeshdict_example}
	\caption{OpenFOAM blockMeshDict Utility}
	\label{of_blockmeshdict}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.3]{Figures/openfoam_model_specs.png}
	\caption{OpenFOAM Model Dimension}
	\label{of_model_specs}
\end{figure}

Figure \ref{of_model_specs} is the specification of every model used in this study. As shown, the length and width of the domain are 32.5D and 2.5D respectively, where D is the length of the square cylinder. Furthermore, boundary conditions were set on the left, right, top , bottom of the domain and also for the two square cylinders. For the velocity boundary conditions, the right, top, and bottom were set to zero gradient while the left side of the domain and the two square cylinders were set to a fixed value of 50 m/s and 0 m/s respectively. For the pressure boundary condition, everything was set to zero gradient except for the right side of the domain where it was set to have a fixed value of 0 Pa.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.225]{Figures/of_mesh_surface_example_v2.png}
	\caption{Mesh Surface View}
	\label{of_mesh}
\end{figure}

\begin{figure}[H]
	\centering
	\includesvg[scale=0.3]{Figures/of_mesh_surface_edges_example.svg}
	\caption{Mesh Surface with Edges View}
	\label{of_mesh_edges}
\end{figure}

Figure \ref{of_mesh} above is an example of one of the many models used in this study. It is generated through reading the blockMeshDict file, which contains the vertices, block parameters, as well as the faces that were already defined. Figure \ref{of_mesh_edges} is a representation of the exact same model as the one in Figure \ref{of_mesh}; the difference is that it shows the individual cells that make up the model. Furthermore, it can also be seen that as the cells get closer to the square cylinders, their size gets smaller and smaller. This is called grading in OpenFOAM. The purpose of it is to make the mesh finer near the square cylinders in order for the solver to be able to calculate the flow parameters more accurately. The size of the computational domain (the gray area in Figure \ref{of_mesh}) is $1.3\; \si{\metre} \times 1.0\; \si{\metre}$. Furthermore, each model consists of 200 cells in the $\mathit{x}$ direction and 240 cells in the $\mathit{y}$ direction, which are graded accordingly to make the mesh finer near the square cylinders as seen on Figure \ref{of_mesh_edges}.

Every model used in this study is composed of several blocks stacked on top of one another until a shape similar to the one shown in Figure \ref{of_mesh} is achieved. The sketch of each block is shown in Figure \ref{openfoam_model_block} and the size of the square cylinders used for every single model is kept constant at $d = 0.04\; \si{\metre}$.

\begin{figure}[H]
	\centering
	\includesvg[scale=0.5]{Figures/openfoam_model_block.svg}
	\caption{Building Blocks of the Mesh}
	\label{openfoam_model_block}
\end{figure}

%---------------------------------------------------------------------
\section{Preprocessing} \label{preprocessing}
% ---------------------------------------------------------------------

The next step after model generation is preprocessing. This includes the solver selection and initialization of the initial conditions mentioned in section \ref{overview}.

In this study, the solver used is the pimpleFoam solver which is normally used for unsteady and incompressible flow conditions. Furthermore, the turbulence model used do conduct the simulation is the RANS $\mathit{k}$-$\omega$ SST model. The initial conditions are set to a constant $u = 50\; \mathrm{m/s}$, $\mathit{k} = 0.375$, and $\omega = 28$ at sea level.

Since OpenFOAM will save every simulation time step, the controlDict utility needs to be properly filled in so that users have control over the number of time steps that are saved in order to save storage space. Users also need to specify when the simulation should stop in the controlDict to prevent the solver from running without stopping. In addition to that, the fvSchemes utility needs to be set up as well as it will be responsible for the discretization of governing equations. Figures \ref{of_controldict} and \ref{of_fvscheme} below are examples of what the controlDict and fvSchemes utility look like.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.4]{Figures/openfoam_controldict_example}
	\caption{OpenFOAM controlDict Utility}
	\label{of_controldict}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.4]{Figures/openfoam_fvscheme_example}
	\caption{OpenFOAM fvSchemes Utility}
	\label{of_fvscheme}
\end{figure}

%---------------------------------------------------------------------
\section{Simulation} \label{section_simulation}
% ---------------------------------------------------------------------

After setting the initial conditions and solver for every model that was generated, the next part is to run the simulation. As mentioned in section \ref{preprocessing}, the simulation is done using the $\mathit{k}$-$\omega$ SST turbulence model. 

Moreover, as mentioned in section \ref{preprocessing}, the controlDict utility is used to set when the simulation need to stop running. In order to know when the simulation can stop running, it needs to reach convergence first. Since users might not have a good idea when the result is going to converge, one of the methods is to plot a graph while the simulation is still running. For this study, a plot of the aerodynamic coefficients was used to see the convergence.

Figure \ref{of_converge_example} shows a plot of the three aerodynamic coefficients over time. As seen there, convergence is achieved at $\mathrm{t} \approx 0.01\;\mathrm{s}$ because the plot of the aerodynamic coefficients start to oscillate after passing that point of time. This behavior is expected because as mentioned previously in section \ref{kvs_section}, vortex shedding is a repeating pattern of vortices that happens at high $\mathit{Re}$, which means that it is oscillating. As a result, it is going to affect the aerodynamic coefficients to behave in a similar manner.

\begin{figure}[H]
	\centering
	\includesvg[scale=0.6]{Figures/RANS_gr2_pimple_Re100_vel50_aero_coeff_plot.svg}
	\caption{Simulation Convergence}
	\label{of_converge_example}
\end{figure}

%---------------------------------------------------------------------
\subsection{RANS $\mathit{k}$-$\omega$ SST Turbulence Model}
% --------------------------------------------------------------------
The $\mathit{k}$-$\omega$ SST turbulence model is widely applied for a variety of applications in aerodynamics. It is classified as a two-equation eddy-viscosity model. In addition, it is a model that make use of both the Wilcox $\mathit{k}$-$\omega$ and $\mathit{k}$-$\epsilon$ models. It utilizes a blending function known as F1 to activate the $\mathit{k}$-$\omega$ model close to the wall and the $\mathit{k}$-$\epsilon$ model in the freestream. The reason for doing this is due to the fact that \cite{autodesk_rans_komegasst}:

\begin{enumerate}
	\item The $\mathit{k}$-$\omega$ model is suited for conducting simulations of flow in the viscous sub-layer.
	\item The $\mathit{k}$-$\epsilon$ model is compatible for the prediction of flow behavior in regions far from the wall.
\end{enumerate}

Furthermore, additional information about the RANS $\mathit{k}$-$\omega$ SST turbulence model are:

\begin{enumerate}
	\item This model show a lower sensitivity to freestream conditions than most models for turbulence.
	\item The shear stress limiter aids the $\mathit{k}$-$\omega$ model to prevent a build-up of an extreme amount of turbulent kinetic energy near stagnation points.
\end{enumerate}

%---------------------------------------------------------------------
\subsubsection{RANS $\mathit{k}$-$\omega$ SST Model $\mathit{k}$ Parameter}
% --------------------------------------------------------------------

The $\mathit{k}$ parameter is one of the parameters that is used in the turbulence model and is known as the turbulence kinetic energy \cite{openfoam_komega}. It can be obtained by using the equation below 

\begin{equation} \label{rans_k_param}
	\large{k = \frac{3}{2} 
		\left( 
			I \left|\mathbf{u}_{ref}\right|
		 \right)^2}
\end{equation}

Where $\mathit{I}$ represents the intensity, and $\mathit{u_{ref}}$ denotes the reference velocity.

%---------------------------------------------------------------------
\subsubsection{RANS $\mathit{k}$-$\omega$ SST Model $\omega$ Parameter}
% --------------------------------------------------------------------

The $\omega$ parameter is also one of the parameters that is used in the turbulence model and is known as the turbulence specific rate of dissipation \cite{openfoam_komega}. It is estimated as follows

\begin{equation} \label{rans_omega_param}
	\large{\omega = \frac{k^{0.5}}{C_{\mu}^{0.25} L}}
\end{equation}

Where the constant $\mathit{C_{\mu} = \mathrm{0.09}}$, and $\mathit{L}$ represents the model's reference length.

%---------------------------------------------------------------------
\section{Postprocessing}
% ---------------------------------------------------------------------

After the simulation is done, the next step is to do postprocessing as needed since as mentioned in section \ref{overview}, the data obtained is further modified or transformed, which means that often times, the result does not help users to understand about what are the things that are happening around the model.

As an example of what postprocessing is, the graph seen in Figure \ref{of_converge_example} is actually an example of one. This is because the aerodynamic coefficients are not possible to compute without the values obtained from the simulation. However, instead of computing its values after the simulation ended, it was computed while the simulation was still running by setting several things in the controlDict utility.

Since the result of the simulation only provide the velocity and pressure values, several other variables such as the $\mathit{u_{mean}}$, $\mathit{u_{rms}}$, and $\mathit{v_{rms}}$ values are to be computed via OpenFOAM's postprocessing functions in this study. 