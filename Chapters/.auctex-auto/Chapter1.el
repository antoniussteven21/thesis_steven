(TeX-add-style-hook
 "Chapter1"
 (lambda ()
   (LaTeX-add-labels
    "section_background"
    "car_picture"
    "building_picture"
    "world_trade_center"
    "train_pantograph"
    "ch1-flowregime"
    "research_objective"))
 :latex)

