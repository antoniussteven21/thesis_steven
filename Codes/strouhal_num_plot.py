import pandas as pd 
import matplotlib.pyplot as plt 
import numpy as np

def calcStrouhal(t1, t2, U=50, d=0.04):
    delta_time = t2 - t1
    freq = 1 / delta_time
    st = (freq * d) / U
    return st

dictionary_Re20e3 = {'gap_ratio':[1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7],
                     'strouhal_num':[0.157, 0.148, 0.153, 0.148, 0.145, 0.148, 0.140, 0.145, 0.145, 0.143, 0.145, 0.140, 0.145]}

dictionary_Re100 = {'gap_ratio':[1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7],
                    'strouhal_num':[0.160, 0.170, 0.163, 0.160, 0.160, 0.157, 0.154, 0.154, 0.154, 0.154, 0.148, 0.148, 0.148]}

dictionary_Re1e3 = {'gap_ratio':[1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7],
                    'strouhal_num':[0.163, 0.143, 0.140, 0.137, 0.133, 0.135, 0.133, 0.136, 0.133, 0.138, 0.130, 0.136, 0.140]}

dictionary_Re1e4 = {'gap_ratio':[1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7], # gr 4, st 0.133
                    'strouhal_num':[0.163, 0.154, 0.148, 0.148, 0.151, 0.145, 0.148, 0.138, 0.143, 0.138, 0.145, 0.145, 0.138]}

dictionary_Re5e3 = {'gap_ratio':[1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7],
                    'strouhal_num':[0.157, 0.157, 0.154, 0.142, 0.151, 0.140, 0.148, 0.145, 0.143, 0.148, 0.145, 0.143, 0.140]}

dictionary_inoue = {'gap_ratio':[2, 3, 4, 5, 6],
                    'strouhal_num':[0.163,0.160,0.158,0.156,0.155]}

dictionary_nakamura = {'gap_ratio': [1, 1.5, 2],
                       'strouhal_num': [0.130, 0.125, 0.120]}

df_Re2e4 = pd.DataFrame(dictionary_Re20e3)
df_Re1e4 = pd.DataFrame(dictionary_Re1e4)
df_Re1e3 = pd.DataFrame(dictionary_Re1e3)
df_Re100 = pd.DataFrame(dictionary_Re100)
df_Re5e3 = pd.DataFrame(dictionary_Re5e3)
df_inoue = pd.DataFrame(dictionary_inoue)
df_nakamura = pd.DataFrame(dictionary_nakamura)

#plot st graph
fig, ax = plt.subplots()
plt.plot(df_Re100.gap_ratio, df_Re100.strouhal_num, color='green', marker='D', label='$Re = 100$')
plt.plot(df_Re1e3.gap_ratio, df_Re1e3.strouhal_num, color='red', marker='^', label='${Re = 1.0 x 10^3}$')
plt.plot(df_Re5e3.gap_ratio, df_Re5e3.strouhal_num, color='magenta', marker='x', label='${Re = 5.0 x 10^3}$')
plt.plot(df_Re1e4.gap_ratio, df_Re1e4.strouhal_num, color='blue', marker='o', label='${Re = 1.0 x 10^4}$')
plt.plot(df_Re2e4.gap_ratio, df_Re2e4.strouhal_num, color='black', marker='s', label='${Re = 2.0 x 10^4}$')
plt.plot(df_inoue.gap_ratio, df_inoue.strouhal_num, color='brown', marker='v', label='${Re = 150}$ (Inoue)')
plt.plot(df_nakamura.gap_ratio, df_nakamura.strouhal_num, color='orange', marker='o', label='${Re = 1.0 x 10^4}$ (Nakamura)')
plt.xlabel('${L/D}$')
plt.ylabel('$St$')
plt.ylim(0.08, 0.18)
plt.legend()
plt.grid()
plt.show()


# save plot as svg
image_name = 'strouhal_number_comparison_all.svg'
image_format = 'svg'
fig.savefig('/home/asteven21/thesis_steven/Figures/' + image_name, format=image_format, dpi=1200)
print('Image: ' + image_name + ' successfully saved')
