import pandas as pd 
import matplotlib.pyplot as plt 


folder_name = "RANS_gr1_pimple_Re5e3_vel50"

# forcesCoeffs
forceCoeffs_path_bot = "/home/asteven21/OpenFOAM/asteven21-10/run/sim_results/"+folder_name+"/postProcessing/forceCoeffs_object_square_bot/0/forceCoeffs.dat"
forceCoeffs_path_top = "/home/asteven21/OpenFOAM/asteven21-10/run/sim_results/"+folder_name+"/postProcessing/forceCoeffs_object_square_top/0/forceCoeffs.dat"

## read the file
df_forceCoeff_sqtop = pd.read_csv(forceCoeffs_path_top, sep="\t", skiprows=9, names=["time","Cm","Cd","Cl","Cl(f)","Cl(r)"])
df_forceCoeff_sqbot = pd.read_csv(forceCoeffs_path_bot, sep="\t", skiprows=9, names=["time","Cm","Cd","Cl","Cl(f)","Cl(r)"])

## plot the graph
fig, ax = plt.subplots()
plt.plot(df_forceCoeff_sqbot["time"], df_forceCoeff_sqbot["Cl"], color="blue", label="$C_{L}$")
plt.plot(df_forceCoeff_sqbot["time"], df_forceCoeff_sqbot["Cd"], color="red", label="$C_{D}$")
plt.plot(df_forceCoeff_sqtop["time"], df_forceCoeff_sqtop["Cl"], color="green", label="$C_{L}$", linestyle="--")
plt.plot(df_forceCoeff_sqtop["time"], df_forceCoeff_sqtop["Cd"], color="black", label="$C_{D}$", linestyle="--")
plt.ylim(-1.3, 1.3)
plt.xlim(right=0.1)
plt.ylabel("$C_{L}$ $C_{D}$")
plt.xlabel("Time (s)")
plt.legend()
plt.grid()

image_name = 'openfoam_convergence_example.svg'
image_format = 'svg'
fig.savefig('/home/asteven21/thesis_steven/Figures/' + image_name, format=image_format, dpi=1200)