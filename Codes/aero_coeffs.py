import pandas as pd 
import matplotlib.pyplot as plt
import os 
import re

os.chdir('/home/asteven21/OpenFOAM/asteven21-10/run/sim_results/')
all_cases = os.listdir()
folder_names = []

for case in all_cases:
    if not re.search('RANS_gr0.5.+', case):
        folder_names.append(case)

folder_names = sorted(folder_names)


for folder_name in folder_names:
    bot_square_file = '/home/asteven21/OpenFOAM/asteven21-10/run/sim_results/'+folder_name+'/postProcessing/forceCoeffs_object_square_bot/0/forceCoeffs.dat'
    top_square_file = '/home/asteven21/OpenFOAM/asteven21-10/run/sim_results/'+folder_name+'/postProcessing/forceCoeffs_object_square_top/0/forceCoeffs.dat'

    # read files
    df_bs = pd.read_csv(bot_square_file, sep="\t", skiprows=9, 
                        names=["time","Cm","Cd","Cl","Cl(f)","Cl(r)"])
    df_ts = pd.read_csv(top_square_file, sep="\t", skiprows=9, 
                        names=["time","Cm","Cd","Cl","Cl(f)","Cl(r)"])

    # plot graph
    fig, ax = plt.subplots()
    ## bottom square
    plt.plot(df_bs.time, df_bs.Cl, color="blue", label="$C_{L} bot$")
    plt.plot(df_bs.time, df_bs.Cd, color="red", label="$C_{D} bot$")

    ## top square
    plt.plot(df_ts.time, df_ts.Cl, color="green", label="$C_{L} top$", 
             linestyle="--")
    plt.plot(df_ts.time, df_ts.Cd, color="black", label="$C_{D} top$", 
             linestyle="--")

    ## plot formatting
    plt.ylim(-1.3, 1.3)
    plt.xlim(0, 0.05)
    plt.ylabel("$C_{L}$ $C_{D}$")
    plt.xlabel("Time (s)")
    plt.legend(loc='lower left')
    plt.grid()
    
    # save plot as svg
    image_name = folder_name + '_aero_coeff_plot.svg'
    image_format = 'svg'
    fig.savefig('/home/asteven21/OpenFOAM/asteven21-10/run/sim_results/'+folder_name+'/'+image_name, format=image_format, dpi=1200)
    fig.savefig('/home/asteven21/thesis_steven/Figures/'+image_name, format=image_format, dpi=1200)
    print('Image: ' + folder_name + ' has been saved')