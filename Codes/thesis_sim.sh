
# script for automating the simulation process for simulation cases

for folder in RANS_gr*_pimple_Re1e3_*/;
do  
    if [[ $folder == RANS_gr0.5*/ ]]
    	then continue;
    fi
    
    cd $folder;
    #open system/blockMeshDict;
    foamCleanCase;
    cd 0;
    rm *.gz;
    cd ..;
    blockMesh;
    decomposePar;
    mpirun -np 4 pimpleFoam -parallel | tee log.Results;
    reconstructPar;
    postProcess -func vorticity;
    postProcess -func writeCellCentres;
    rm -r processor*
    cd ..;
done
